export interface BlogTask {
  id: string;
  summary: string;
  status: string;
}
