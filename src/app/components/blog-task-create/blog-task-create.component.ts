import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {MatSnackBar} from "@angular/material/snack-bar";
import {BlogTaskService} from "../../services/blog-task.service";
import {BlogTask} from "../../model/blog-task";

@Component({
  selector: 'app-blog-task-create',
  templateUrl: './blog-task-create.component.html',
  styleUrls: ['./blog-task-create.component.scss']
})
export class BlogTaskCreateComponent implements OnInit {
  blogTaskCreateForm = new FormGroup({
    summary: new FormControl(null, [Validators.required])
  });

  get summaryControl(): FormControl { return this.blogTaskCreateForm.controls.summary as FormControl; }

  constructor(private readonly snackBar: MatSnackBar, private readonly taskSvc: BlogTaskService) { }

  ngOnInit(): void {
  }

  cancel() {
    this.blogTaskCreateForm.reset();
  }

  save() {
    this.validate();
    if (!this.blogTaskCreateForm.valid) {
      this.snackBar.open('Task is invalid, cannot save!', null, {
        duration: 2000
      })
      return;
    }
    this.taskSvc.create(this.blogTaskCreateForm.value as BlogTask).subscribe(
      () => {
        this.snackBar.open("Task saved successfully", null, {
          duration: 1000
        });
      },
      () => {
        this.snackBar.open("Saving task failed", null, {
          duration: 2000
        });
      }
    );
  }

  private validate() {
    this.blogTaskCreateForm.controls.summary.markAsTouched();
  }
}
