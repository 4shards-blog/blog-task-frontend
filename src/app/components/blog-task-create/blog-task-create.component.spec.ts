import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogTaskCreateComponent } from './blog-task-create.component';

describe('BlogTaskCreateComponent', () => {
  let component: BlogTaskCreateComponent;
  let fixture: ComponentFixture<BlogTaskCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlogTaskCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogTaskCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
