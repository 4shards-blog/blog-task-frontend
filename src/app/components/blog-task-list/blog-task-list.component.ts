import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {BlogTaskService} from "../../services/blog-task.service";
import {BlogTask} from "../../model/blog-task";

@Component({
  selector: 'app-blog-task-list',
  templateUrl: './blog-task-list.component.html',
  styleUrls: ['./blog-task-list.component.scss']
})
export class BlogTaskListComponent implements OnInit {

  tasks: Observable<BlogTask[]>;
  taskCount: number;
  finishedCount: number;

  constructor(public readonly taskSvc: BlogTaskService) {
    this.tasks = taskSvc.tasks;
    this.tasks.subscribe(tasks => {
      this.taskCount = tasks.length;
      this.finishedCount = tasks.filter((task) => task.status === 'done').length;
    });
  }

  ngOnInit(): void {
  }
}
