import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogTaskListComponent } from './blog-task-list.component';

describe('BlogTaskListComponent', () => {
  let component: BlogTaskListComponent;
  let fixture: ComponentFixture<BlogTaskListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlogTaskListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogTaskListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
