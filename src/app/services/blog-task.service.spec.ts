import { TestBed } from '@angular/core/testing';

import { BlogTaskService } from './blog-task.service';

describe('BlogTaskService', () => {
  let service: BlogTaskService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BlogTaskService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
