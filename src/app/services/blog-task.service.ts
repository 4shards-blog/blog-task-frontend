import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {BlogTask} from "../model/blog-task";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class BlogTaskService {

  private tasks$ = new BehaviorSubject<BlogTask[]>([]);

  get tasks(): Observable<BlogTask[]> { return this.tasks$.asObservable();  }

  constructor(private readonly http: HttpClient) {
    this.updateTasks();
  }

  private updateTasks() {
    this.http
      .get<BlogTask[]>(`${environment.apiEndpointRoot}/tasks`)
      .subscribe(
        result => this.tasks$.next(result),
      );
  }

  create(value: BlogTask): Observable<any> {
    return new Observable<boolean>(observer => {
      this.http.post(`${environment.apiEndpointRoot}/tasks`, value).subscribe(
        () => {
          this.updateTasks();
          observer.next();
        },
        () => {
          observer.error();
        });
    });
  }

  updateStatus(taskId: string, status: string): Observable<any> {
    return new Observable<any>(observer => {
      this.http.patch(`${environment.apiEndpointRoot}/tasks/${taskId}`, {
        status: status
      }).subscribe(
        () => {
          this.updateTasks();
          observer.next();
        },
        () => observer.error()
      )
    })
  }
}
